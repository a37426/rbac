package pt.ipl.isel.meic.si.tp3.repository;

import java.io.Serializable;

import javax.persistence.EntityManager;

public interface Dao<T extends Serializable, PK extends Serializable> {
	public EntityManager getEntityManager();

	public T create(T t);

	public T read(PK id);

	public T update(T t);

	public void delete(T t);
}
